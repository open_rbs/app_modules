package com.app.modules.classes.background_module;

import android.app.Notification;
import android.content.Intent;
import android.util.Log;

import com.app.modules.classes.Module.IModule;
import com.app.modules.classes.Module.Module;
import com.app.modules.classes.common.Callback;
import com.app.modules.classes.storage_module.StorageModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * <p>Класс - модуль для работы с фоновым режимом</p>
 * <p>С его помощью приложение остаеться работать на заднем плане, когда его скроют</p>
 * <p>Класс наследует Module и реализует интерфейс IModule</p>
 * <p>Реализует паттерн синглтон - должен быть всего 1 екземпляр каждого модуля</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 * @see Module
 * @see IModule
 */
public class BackgroundModule extends Module implements IModule{

    /**
     * Инициализация концигураций для уведомлений
     */
    private static final String ICON = "";
    private static final String TITLE = "";
    private static final String TEXT = "";

    /**
     * Уникальный айди увидомления
     */
    public static final int ID = 1;

    /**
     * Сервис для показа уведомлений
     */
    private BgService service;

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "BACKGROUND_MODULE";

    /**
     * <p>Инстанс данного класса</p>
     */
    private static BackgroundModule instance;

    /**
     * <p>Приватный конструктор для реализации синглтона</p>
     */
    private BackgroundModule(){

    }

    /**
     * <p>Нужно ли активировать задний фон при скрытие приложения</p>
     */
    private boolean active = false;

    /**
     * <p>Метод для реализации синглтона</p>
     * @return каждый модуль должен возвращать свой инстанс в виде своего класса
     */
    public static BackgroundModule getInstance() {
        if(instance == null){
            instance = new BackgroundModule();
        }
        return instance;
    }

    /**
     * <p>Запуск сервиса</p>
     */
    public void startService(){

    }

    /**
     * <p>Остановка сервиса</p>
     */
    public void stopService(){

    }

    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */
    @Override
    public void pause() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    @Override
    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */
    @Override
    public void resume() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    @Override
    public void destroy() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    public void activityResult(int requestCode, int resultCode, Intent data){

    }


    /**
     * <p>Входная точка для выполнения действия на подобии CLI.
     * Обрабатываються только действия с определенными названиями по сценариям.</p>
     * @param cb Коллбек, который вызываеться после того как модуль выполнит необходимые действия.
     * @param actionName Название действия, которое соответствует полю из перечислением
     * @param args Аргументы к действию
     * @see StorageModule.Action
     */
    @Override
    public void execute(Callback cb, String actionName, JSONArray args) {
        //родительскому классу присваивается коллбек
        this.callback = cb;

        //пустое действие по умолчанию
        BackgroundModule.Action action = BackgroundModule.Action.Empty;

        //пытаемся связать строковое название действия с перечислением
        try {
            action = BackgroundModule.Action.valueOf(actionName);
        } catch (IllegalArgumentException e) {
            Log.e("PLUGIN_BG", "unexpected error", e);
        }

        //выполняем действие
        try{
            //Извлекаем обьект из аргументов
            JSONObject obj = args.getJSONObject(0);

            //определяем нужный кейс
            switch(action){
                case enable:
                    activate();
                    cb.success();
                    break;
                case disable:
                    deactivate();
                    cb.success();
                    break;
            }
        }catch(JSONException e){
            e.printStackTrace();
            //в случае ошибки сообщаем в коллбек
            cb.error("Some error with action " + actionName);
        }
    }

    public void activate(){
        active = true;
    }

    public void deactivate(){
        active = false;
    }

    /**
     * <p>Перечисление для работы с действиями</p>
     */
    enum Action {
        enable,
        disable,
        Empty
    }

    class BgService{
        /**
         * <p>Показывает уведомление</p>
         */
        private void awake(){

        }

        /**
         * <p>Закрывает уведомление</p>
         */
        private void sleep(){

        }

        /**
         * <p>Создает уведомления</p>
         * @return Возвращает уведомление для приложения
         */
        private Notification makeNotification(){
            return null;
        }
    }
}
