package com.app.modules.classes.common;

import android.app.Activity;

import com.app.modules.classes.Module.IModule;
import com.app.modules.classes.Module.Module;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * <p>Класс для обмена мообщениями между системой и модулями</p>
 * <p>С его помощью модули отвечают на запросы и генерируют евенты</p>
 */
public class Callback {

    /**
     * Типы данных, которые передаются и могут обрабатываться системой
     */

    /**
     * Пустая строка
     */
    private static final int SIMPLE = 1;
    /**
     * Строка строка
     */
    private static final int STRING = 2;
    /**
     * JSONObject
     */
    private static final int JSON_OBJ = 3;
    private static final int JSON_ARR = 4;

    private Activity a;

    /**
     * Уникальный идентификатор
     */
    private String myId;

    /**
     * Активен ли коллобек
     */
    private boolean active;

    public Callback(String _id, Activity _a){
        this.myId = _id;
        this.a = _a;
    }

    /**
     * Деактивирует коллобек
     */
    public void disable(){

    }


    /**
     * Успешный ответ - вызывает response
     */
    public void success(){

    }

    /**
     * Успешный ответ - вызывает response
     */
    public void success(String value){

    }

    /**
     * Успешный ответ - вызывает response
     */
    public void success(JSONArray arr, boolean disableReplacing){
        // String s = disableReplacing ? arr.toString() : arr.toString().replace("\"", "\\\"");
    }

    /**
     * Успешный ответ - вызывает response
     */
    public void success(JSONObject obj){
        //String s = obj.toString().replace("\"", "\\\"");
    }

    /**
     * Ошибка-ответ - вызывает response c ERR_STATUS
     */
    public void error(String value){

    }

    /**
     * Вызывает метод системы со всеми необходимыми параметрами если данный колбек еще активен
     * @param responseType Тип взятый из констант этого класса
     * @param status Значение констаны OK_STATUS || ERR_STATUS
     * @param message Сам ответ в виде строки
     */
    public void response(int responseType, String status, String message){

    }

    /**
     * Генерирует евент - вызывает event
     */
    public void cb(String module, String action){

    }

    /**
     * Генерирует евент - вызывает event
     */
    public void cb(String module, String action, String value){

    }

    /**
     * Генерирует евент - вызывает event
     */
    public void cb(String module, String action, JSONObject obj){
        //String s = obj.toString().replace("\"", "\\\"");
    }

    /**
     * Вызывает метод системы со всеми необходимыми параметрами если данный колбек еще активен
     * @param module Имя модуля, который генерирует евент
     * @param action Название евента
     * @param resposnseType Тип ответа, одна из констант
     * @param message  Сообщение в евенте
     */
    private void event(String module, String action, int resposnseType, String message){

    }

    /**
     * @return Возвращает уникальный идентификатор коллбека
     */
    public String getMyId(){
        return this.myId;
    }
}
