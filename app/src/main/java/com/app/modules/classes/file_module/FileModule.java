package com.app.modules.classes.file_module;

import android.content.Intent;
import android.util.Log;

import com.app.modules.classes.Module.IModule;
import com.app.modules.classes.Module.Module;
import com.app.modules.classes.common.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * <p>Класс - модуль для работы с ассетами (assets) и хранилищем приложения (external storage)</p>
 * <p>С его помощью будут перемещаться ассеты в хранилище, добавляться, изменяться, удаляться файлы
 * по пути file://data/data/{пакет приложения}</p>
 * <p>Класс наследует Module и реализует интерфейс IModule</p>
 * <p>Реализует паттерн синглтон - должен быть всего 1 екземпляр каждого модуля</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 * @see Module
 * @see IModule
 */
public class FileModule extends Module implements IModule {

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "FILE_MODULE";

    /**
     * <p>путь к корню хранилища данного приложения на девайсе</p>
     */
    private String rootPath;

    /**
     * классы для работы с хранилищем
     */
    private Reader reader;
    private Writer writer;

    /**
     * <p>инстанс данного класса</p>
     */
    private static FileModule instance;

    /**
     * <p>Приватный конструктор для реализации синглтона</p>
     */
    private FileModule(){
        //тут присвоить значение корня папкихранилища приложения в rootPath
        //инициализация ридера и врайтера
        //TO DO
    }

    public Reader getReader(){
        //TO DO
        return null;
    }

    public Writer getWriter(){
        //TO DO
        return null;
    }

    /**
     * <p>Метод для реализации синглтона</p>
     * @return каждый модуль должен возвращать свой инстанс в виде своего класса
     */
    public static FileModule getInstance() {
        if(instance == null){
            instance = new FileModule();
        }
        return instance;
    }


    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */
    @Override
    public void pause() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    @Override
    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */
    @Override
    public void resume() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    @Override
    public void destroy() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    public void activityResult(int requestCode, int resultCode, Intent data){

    }

    /**
     * <p>Входная точка для выполнения действия на подобии CLI.
     * Обрабатываються только действия с определенными названиями по сценариям.</p>
     * @param cb Коллбек, который вызываеться после того как модуль выполнит необходимые действия.
     * @param actionName Название действия, которое соответствует полю из перечислением
     * @param args Аргументы к действию
     * @see Action
     */
    @Override
    public void execute(Callback cb, String actionName, JSONArray args) {
        //родительскому классу присваивается коллбек
        this.callback = cb;

        //пустое действие по умолчанию
        Action action = Action.Empty;

        //пытаемся связать строковое название действия с перечислением
        try {
            action = Action.valueOf(actionName);
        } catch (IllegalArgumentException e) {
            Log.e("PLUGIN_"+NAME, "unexpected error", e);
        }

        //выполняем действие
        try{
            //Извлекаем обьект из аргументов
            JSONObject obj = args.getJSONObject(0);
            String path =  obj.getString("path");

            //определяем нужный кейс
            switch(action){
                case exists:
                    cb.success(reader.fileExists(path)+"");
                    break;
                case realpath:
                    cb.success(reader.systemPath(path));
                    break;
                case unlink:
                    writer.deleteFile(path);
                    cb.success();
                    break;
                case mkdir:
                    writer.createFolder(path);
                    cb.success();
                    break;
                case open:
                    writer.createOrOpen(path);
                    cb.success();
                    break;
                case isDirectory:
                    cb.success(reader.isDir(path)+"");
                    break;
                case writeFile:
                    String data =  obj.getString("data");
                    if(!obj.getBoolean("needBase64")){
                        writer.writeFile(data, path);
                    }else{
                        writer.writeBlob(data, path);
                    }
                    cb.success();
                    break;
                case rmdir:
                    writer.removeFolder(path);
                    cb.success();
                    break;
            }
        }catch(JSONException e){
            e.printStackTrace();
            //в случае ошибки сообщаем в коллбек
            cb.error("Some error with action " + actionName);
        }
    }

    /**
     * <p>Перечисление для работы с действиями</p>
     */
    enum Action {
        exists,
        realpath,
        unlink,
        mkdir,
        open,
        isDirectory,
        writeFile,
        rmdir,
        Empty
    }

    /**
     * <p>Класс для работы с операциями чтения файлов и их информации</p>
     * <p>Основные методы работают с хранилищем приложения по пути file://data/data/{пакет приложения}</p>
     * <p>Методы со словом assets работают с ассетами приложения</p>
     */
    public class Reader {

        /**
         * <p>Возвращает реальный путь к сущности в хранилище приложения</p>
         * @param path Относительный путь к папке или файлу, начиная с корня хранилища приложения
         *             к примеру lib/fonts или main.js
         * @return полный путь к файлу включая file://data/data/{пакет приложения}
         */
        public String systemPath(String path){
            //TO DO
            return "";
        }

        /**
         * <p>Проверяет сущность на существование</p>
         * @param path Относительный путь к папке или файлу, начиная с корня хранилища приложения
         *             к примеру lib/fonts или main.js
         * @return true если существует
         */
        public boolean fileExists(String path){
            //TO DO
            return true;
        }

        /**
         * <p>Находит все файлы по данному пути в хранилище приложения и возвращает их имена</p>
         * @param path Относительный путь к папке, начиная с корня хранилища приложения
         *             к примеру lib/fonts
         * @return список имен
         */
        public List<String> getFilesList(String path){
            //TO DO
            return null;
        }

        /**
         * <p>Находит все файлы по данному пути в ассетах и возвращает их имена</p>
         * @param path Относительный путь к папке, начиная с корня ассетов приложения
         *             к примеру lib/fonts
         * @return список имен
         */
        public List<String> getAssetsList(String path){
            //TO DO
            return null;
        }

        /**
         * <p>Считывает ассет по указанному пути и возвращают массив байт</p>
         * @param path Относительный путь к файлу, начиная с корня ассетов приложения
         *             к примеру lib/fonts/roboto.ttf
         * @return массив байт
         */
        public byte[] readAsset(String path){
            //TO DO
            return new byte[1];
        }

        /**
         * <p>Определяет являеться ли сущность по указанному пути файлом</p>
         * @param path Относительный путь к файлу, начиная с корня хранилища приложения
         *             к примеру app/pages/main
         * @return true если файл
         */
        public boolean isFile(String path){
            //TO DO
            return true;
        }

        /**
         * <p>Определяет являеться ли сущность по указанному пути папкой</p>
         * @param path Относительный путь к папке, начиная с корня хранилища приложения
         *             к примеру app/pages/main
         * @return true если папка
         */
        public boolean isDir(String path){
            //TO DO
            return true;
        }
    }

    /**
     * <p>Класс для работы с операциями записи и обновления файлов и их информации</p>
     * <p>Основные методы работают с хранилищем приложения по пути file://data/data/{пакет приложения}</p>
     * <p>Методы с приставкой assets работают с ассетами приложения</p>
     */
    public class Writer {

        /**
         * <p>Создает папку если она не существует по указанному пути</p>
         * @param path Относительный путь к папке, начиная с корня хранилища приложения
         *             к примеру app/pages/main
         * @return true если создано
         */
        public boolean createFolder(String path){
            //TO DO
            return true;
        }

        /**
         * <p>Создает файл если он не существует по указанному пути</p>
         * @param path Относительный путь к файлу, начиная с корня хранилища приложения
         *             к примеру app/pages/main/images/icon.png
         * @return true если создано
         */
        public boolean createOrOpen(String path){
            //TO DO
            return true;
        }

        /**
         * <p>Создает папки если их не существует по указанному пути</p>
         * <p>Парсит строку на папки с разделителем "/" и воздает все папки слева - направо если их не было</p>
         * <p>К примеру с параметром app/pages/main/images в итоге должны существовать все 4 папки</p>
         * @param path Относительный путь к папке, начиная с корня хранилища приложения
         *             к примеру app/pages/main/images
         * @return true если создано
         */
        public boolean circularCreate(String path){
            //TO DO
            return true;
        }

        /**
         * <p>Записывает пришедшую строку в бинарный файл</p>
         * <p>Сначала декодирует base64, потом записывает байты в файл, создав его перед этим, если его не было</p>
         * @param contentBase64 Закодированая base64 строка
         * @param path Относительный путь к файлу, начиная с корня хранилища приложения
         *             к примеру app/pages/main/images/logo.png
         * @return true если создано
         */
        public boolean writeBlob(String contentBase64, String path){
            //TO DO
            return true;
        }

        /**
         * <p>Записывает байты в бинарный файл</p>
         * @param content Байты файла
         * @param path Относительный путь к файлу, начиная с корня хранилища приложения
         *             к примеру app/pages/main/images/logo.png
         * @return true если создано
         */
        public boolean writeFile(byte[] content, String path){
            //TO DO
            return true;
        }

        /**
         * <p>Записывает текст в текстовый файл</p>
         * @param content Строки файла, могут быть с переносами, отступами и тд
         * @param path Относительный путь к файлу, начиная с корня хранилища приложения
         *             к примеру app/js/libs/main.js
         * @return true если создано
         */
        public boolean writeFile(String content, String path){
            //TO DO
            return true;
        }

        /**
         * <p>Удаляет все файлы в папке и саму папку</p>
         * @param path Относительный путь к папке, начиная с корня хранилища приложения
         *             к примеру app/js/lib
         * @return true если удалено
         */
        public boolean removeFolder(String path){
            //TO DO
            return true;
        }

        /**
         * <p>Удаляет файл</p>
         * @param path Относительный путь к файлу, начиная с корня хранилища приложения
         *             к примеру app/js/lib/common.js
         * @return true если удалено
         */
        public boolean deleteFile(String path){
            //TO DO
            return true;
        }
    }
}
