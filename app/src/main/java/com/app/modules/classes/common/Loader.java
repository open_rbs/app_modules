package com.app.modules.classes.common;

import java.util.zip.ZipInputStream;


/**
 * <p>Класс для работы с подгрузкой файлов, их проверкой и распаковкой</p>
 * <p>С его помощью модули отвечают на запросы и генерируют евенты</p>
 */
public class Loader {

    /**
     * Пути и имена файлов
     */
    private static final String SERVER_PATH = "http://somehost.com/file.zip";
    private static final String FILE_NAME = "file.zip";
    private static final String KEY_FILE_NAME = "index.html";
    /**
     * Относительный путь, куда будут распакованы файлы
     */
    private static final String LAUNCHER_PATH = "path";

    /**
     * Пути к файлам в хранилище приложения
     */
    private String rootPath;

    /**
     * file:// + rootPath + KEY_FILE_NAME
     */
    private String localUrl;

    /**
     * Инициализирует пути
     */
    public Loader(){

    }

    /**
     * Проверияет файл из архива на существование и если его нету - то начинает процесс скачивания и распаковки
     */
    public void checkFile(){

    }

    /**
     * Скачивает файл если его нету
     */
    public void loadFile(){

    }

    /**
     * Разархивирует архив
     * @return true если ок
     */
    public boolean unzipFile(){
        return true;
    }

    /**
     * Извлекает файл с архива
     * @param zipIn поток архиватора
     * @param filePath название файла в который будет записываться инфа
     */
    private void extractFile(ZipInputStream zipIn, String filePath){

    }
}
