package com.app.modules.classes.Module;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.app.modules.classes.common.Callback;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Базовый класс модуля, который будут наследовать все модули приложения</p>
 * <p>Класс содержить статические методы, выполнение которых может быть связано
 * с выполнением соответственных методов у всего списка модулей</p>
 */
public class Module {

    private static Activity activity;
    private static List<Module> modules = new ArrayList<Module>();

    public Callback callback;

    /**
     * <p>инициализирует модуль и добавляет его в список модулей</p>
     */
    public Module(){
        //TO DO
    }

    /**
     * <p>Отменяет текущий коллбек</p>
     */
    public void disableCallback(){
        callback.disable();
    }

    /**
     * <p>генерирует евент</p>
     * @param moduleName имя модуля
     * @param event название евента
     * @param data данные к евенту
     */
    public static void once(String moduleName, String event, String data) {
        Module module = getModule(moduleName);
        if(module.callback != null){
            module.callback.cb(moduleName, event, data);
        }
    }

    /**
     * <p>Метод для инициализации данного класса</p>
     * @param _activity главная активность приложения
     */
    public static void init(Activity _activity){
        activity = _activity;
    }

    /**
     * <p>Возвращает модуль по имени</p>
     * @param name имя модуля, который нужно вернуть
     */
    public static Module getModule(String name){
        //TO DO
        return null;
    }

    /**
     * <p>Вызывает у всех модулей метод pause()</p>
     */
    public static void onPause(){
        //TO DO
    }

    /**
     * <p>Вызывает у всех модулей метод create()</p>
     */
    public static void onCreate(){
        //TO DO
    }

    /**
     * <p>Вызывает у всех модулей метод resume()</p>
     */
    public static void onResume(){
        //TO DO
    }

    /**
     * <p>Вызывает у всех модулей метод destroy()</p>
     */
    public static void onDestroy(){
        //TO DO
    }

    /**
     * <p>Вызывает у всех модулей метод activityResult() с параметрами</p>
     */
    public static void onActivityResult(int requestCode, int resultCode, Intent data){
        //TO DO
    }


    /**
     * Метод - адаптер между системой и модулями
     * @param pluginAsString
     * @param action
     * @param id
     * @param params
     */
    public void action(String pluginAsString, String action, String id, String params){


        JSONArray args = null;
        try {
            args = new JSONArray(params);
        } catch (JSONException e) {

        }

        Module m = Module.getModule(pluginAsString);
        if(m instanceof IModule){
            IModule module = (IModule) m;
            if(module != null){
                module.execute(new Callback(id, activity), action, args);
            }else{
                Log.e("MODULE", "No such module registered");
            }
        }else{
            Log.e("MODULE", "No such module registered, instance error");
        }


    }

}
