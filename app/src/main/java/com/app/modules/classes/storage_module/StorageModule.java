package com.app.modules.classes.storage_module;

import android.content.Intent;
import android.util.Log;

import com.app.modules.classes.Module.IModule;
import com.app.modules.classes.Module.Module;
import com.app.modules.classes.ads_module.AdsModule;
import com.app.modules.classes.common.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * <p>Класс - модуль для работы с данными</p>
 * <p>С его помощью сохраняються и загружаються данные для приложения</p>
 * <p>Класс наследует Module и реализует интерфейс IModule</p>
 * <p>Реализует паттерн синглтон - должен быть всего 1 екземпляр каждого модуля</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 * @see Module
 * @see IModule
 */
public class StorageModule extends Module implements IModule{

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "STORAGE_MODULE";

    /**
     * <p>инстанс данного класса</p>
     */
    private static StorageModule instance;

    /**
     * <p>Приватный конструктор для реализации синглтона</p>
     */
    private StorageModule(){

    }

    /**
     * <p>Метод для реализации синглтона</p>
     * @return каждый модуль должен возвращать свой инстанс в виде своего класса
     */
    public static StorageModule getInstance() {
        if(instance == null){
            instance = new StorageModule();
        }
        return instance;
    }

    /**
     * <p>Сохраняет значение по ключу в указанное хранилище</p>
     * @param storageName Название хранилища
     * @param key Ключ по которому будет хранить значение
     * @param value Значение
     */
    public boolean save(String storageName, String key, String value){
        return true;
    }

    /**
     * <p>Возвращает значение по ключу в указанном хранилище</p>
     * @param storageName Название хранилища
     * @param key Ключ по которому будет хранить значение
     */
    public String load(String storageName, String key){
        return "";
    }

    /**
     * <p>Удаляет значение по ключу в указанное хранилище</p>
     * @param storageName Название хранилища
     * @param key Ключ по которому будет хранить значение
     */
    public boolean delete(String storageName, String key){
        return true;
    }

    /**
     * <p>Возвращает все значения в указанном хранилище</p>
     * @param storageName Название хранилища
     */
    public List<String> loadAll(String storageName){
        return null;
    }

    /**
     * <p>Удаляет все значения в указанном хранилище</p>
     * @param storageName Название хранилища
     */
    public boolean deleteAll(String storageName){
        return true;
    }

    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */
    @Override
    public void pause() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    @Override
    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */
    @Override
    public void resume() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    @Override
    public void destroy() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    public void activityResult(int requestCode, int resultCode, Intent data){

    }


    /**
     * <p>Входная точка для выполнения действия на подобии CLI.
     * Обрабатываються только действия с определенными названиями по сценариям.</p>
     * @param cb Коллбек, который вызываеться после того как модуль выполнит необходимые действия.
     * @param actionName Название действия, которое соответствует полю из перечислением
     * @param args Аргументы к действию
     * @see StorageModule.Action
     */
    @Override
    public void execute(Callback cb, String actionName, JSONArray args) {
        //родительскому классу присваивается коллбек
        this.callback = cb;

        //пустое действие по умолчанию
        StorageModule.Action action = StorageModule.Action.Empty;

        //пытаемся связать строковое название действия с перечислением
        try {
            action = StorageModule.Action.valueOf(actionName);
        } catch (IllegalArgumentException e) {
            Log.e("PLUGIN_"+NAME, "unexpected error", e);
        }

        //выполняем действие
        try{
            //Извлекаем обьект из аргументов
            JSONObject obj = args.getJSONObject(0);
            String storage =  obj.getString("storage");
            //определяем нужный кейс
            switch(action){
                case load:
                    String key_load =  obj.getString("key");
                    cb.success(load(storage, key_load));
                    break;
                case save:
                    String key_save =  obj.getString("key");
                    cb.success(delete(storage, key_save) + "");
                    break;
                case delete:
                    String key_delete =  obj.getString("key");
                    cb.success(delete(storage, key_delete) + "");
                    break;
                case loadAll:
                    List<String> list = loadAll(storage);
                    //convert list to JSONArr
                    //cb.success(arr);
                    break;
                case deteleAll:
                    cb.success(deleteAll(storage) + "");
                    break;
            }
        }catch(JSONException e){
            e.printStackTrace();
            //в случае ошибки сообщаем в коллбек
            cb.error("Some error with action " + actionName);
        }
    }


    /**
     * <p>Перечисление для работы с действиями</p>
     */
    enum Action {
        load,
        save,
        delete,
        loadAll,
        deteleAll,
        Empty
    }
}
