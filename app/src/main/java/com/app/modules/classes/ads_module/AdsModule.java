package com.app.modules.classes.ads_module;

import android.content.Intent;
import android.util.Log;

import com.app.modules.classes.Module.IModule;
import com.app.modules.classes.Module.Module;
import com.app.modules.classes.app_module.AppModule;
import com.app.modules.classes.common.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * <p>Класс - модуль для работы с рекламой</p>
 * <p>С его помощью инициализируеться и регулируеться показ рекламы</p>
 * <p>Класс наследует Module и реализует интерфейс IModule</p>
 * <p>Реализует паттерн синглтон - должен быть всего 1 екземпляр каждого модуля</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 * @see Module
 * @see IModule
 */
public class AdsModule extends Module implements IModule{

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "ADS_MODULE";

    /**
     * <p>События генерируемые модулем</p>
     */
    public static final String ON_AD_LOADED = "on_ad_loaded";
    public static final String ON_AD_CLOSED = "on_ad_closed";

    /**
     * <p>Уникальноый идентификатор приложения адмоб</p>
     */
    public static final String APP_ID = "application_admob_id";

    /**
     * <p>инстанс данного класса</p>
     */
    private static AdsModule instance;

    /**
     * <p>Приватный конструктор для реализации синглтона</p>
     */
    private AdsModule(){

    }


    /**
     * <p>Метод для реализации синглтона</p>
     * @return каждый модуль должен возвращать свой инстанс в виде своего класса
     */
    public static AdsModule getInstance() {
        if(instance == null){
            instance = new AdsModule();
        }
        return instance;
    }

    /**
     * Инициализирует междустраничные обьявления и регистрирует слушателей событий
     * @param unit Идентификатор рекламной записи адмоб
     */
    private void init(final String unit){
        //TO DO
    }

    /**
     * Начинает загрузка межстраничного обьявления если оно еще не готово
     */
    private void prepareInterstitial(){
        //TO DO
    }

    /**
     * Показывает обьявление если оно готово к показу
     */
    private void showInterstitial(){
        //TO DO
    }

    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */
    @Override
    public void pause() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    @Override
    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */
    @Override
    public void resume() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    @Override
    public void destroy() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    public void activityResult(int requestCode, int resultCode, Intent data){

    }


    /**
     * <p>Входная точка для выполнения действия на подобии CLI.
     * Обрабатываються только действия с определенными названиями по сценариям.</p>
     * @param cb Коллбек, который вызываеться после того как модуль выполнит необходимые действия.
     * @param actionName Название действия, которое соответствует полю из перечислением
     * @param args Аргументы к действию
     * @see AdsModule.Action
     */
    @Override
    public void execute(Callback cb, String actionName, JSONArray args) {
        //родительскому классу присваивается коллбек
        this.callback = cb;

        //пустое действие по умолчанию
        AdsModule.Action action = AdsModule.Action.Empty;

        //пытаемся связать строковое название действия с перечислением
        try {
            action = AdsModule.Action.valueOf(actionName);
        } catch (IllegalArgumentException e) {
            Log.e("PLUGIN_"+NAME, "unexpected error", e);
        }

        //выполняем действие
        try{
            //Извлекаем обьект из аргументов
            JSONObject obj = args.getJSONObject(0);

            //определяем нужный кейс
            switch(action){
                case init:
                    String unit =  obj.getString("unit");
                    init(unit);
                    cb.success();
                    break;
                case prepareInterstitial:
                    prepareInterstitial();
                    cb.success();
                    break;
                case showInterstitial:
                    showInterstitial();
                    cb.success();
                    break;
                case once:
                    //NOTHING TO DO
                    break;
            }
        }catch(JSONException e){
            e.printStackTrace();
            //в случае ошибки сообщаем в коллбек
            cb.error("Some error with action " + actionName);
        }
    }

    /**
     * <p>Перечисление для работы с действиями</p>
     */
    enum Action {
        init,
        prepareInterstitial,
        showInterstitial,
        once,
        Empty
    }
}
