package com.app.modules.classes.app_module;

import android.content.Intent;
import android.util.Log;

import com.app.modules.classes.Module.IModule;
import com.app.modules.classes.Module.Module;
import com.app.modules.classes.common.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * <p>Класс - модуль для работы с приложением</p>
 * <p>С его помощью будет возможность взаимодействовать с приложением</p>
 * <p>Класс наследует Module и реализует интерфейс IModule</p>
 * <p>Реализует паттерн синглтон - должен быть всего 1 екземпляр каждого модуля</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 * @see Module
 * @see IModule
 */
public class AppModule extends Module implements IModule{

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "APP_MODULE";

    /**
     * <p>инстанс данного класса</p>
     */
    private static AppModule instance;

    /**
     * <p>Приватный конструктор для реализации синглтона</p>
     */
    private AppModule(){

    }

    /**
     * <p>Метод для реализации синглтона</p>
     * @return каждый модуль должен возвращать свой инстанс в виде своего класса
     */
    public static AppModule getInstance() {
        if(instance == null){
            instance = new AppModule();
        }
        return instance;
    }

    /**
     * <p>Открывает интент по юри (может быть почта, может быть урл в браузер)</p>
     * @param uri Путь передаваемый интенту
     */
    private void loadUrl(String uri){
        //TO DO
    }

    /**
     * <p>Возвращает локализацию в виде ru-RU, en-US</p>
     */
    private String getLocale(){
        //TO DO
        return "";
    }

    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */
    @Override
    public void pause() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    @Override
    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */
    @Override
    public void resume() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    @Override
    public void destroy() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    public void activityResult(int requestCode, int resultCode, Intent data){

    }

    /**
     * <p>Входная точка для выполнения действия на подобии CLI.
     * Обрабатываються только действия с определенными названиями по сценариям.</p>
     * @param cb Коллбек, который вызываеться после того как модуль выполнит необходимые действия.
     * @param actionName Название действия, которое соответствует полю из перечислением
     * @param args Аргументы к действию
     * @see AppModule.Action
     */
    @Override
    public void execute(Callback cb, String actionName, JSONArray args) {
        //родительскому классу присваивается коллбек
        this.callback = cb;

        //пустое действие по умолчанию
        AppModule.Action action = AppModule.Action.Empty;

        //пытаемся связать строковое название действия с перечислением
        try {
            action = AppModule.Action.valueOf(actionName);
        } catch (IllegalArgumentException e) {
            Log.e("PLUGIN_"+NAME, "unexpected error", e);
        }

        //выполняем действие
        try{
            //Извлекаем обьект из аргументов
            JSONObject obj = args.getJSONObject(0);

            //определяем нужный кейс
            switch(action){
                case loadUrl:
                    String uri =  obj.getString("uri");
                    loadUrl(uri);
                    cb.success();
                    break;
                case locale:
                    cb.success(getLocale());
                    break;
                case appInstalled:
                    String pack =  obj.getString("pack");
                    cb.success(appInstalled(pack)+"");
                    break;
                case openInApp:
                    String p =  obj.getString("pack");
                    String u =  obj.getString("uri");
                    openInApp(p, u);
                    cb.success();
                    break;
            }
        }catch(JSONException e){
            e.printStackTrace();
            //в случае ошибки сообщаем в коллбек
            cb.error("Some error with action " + actionName);
        }
    }

    /**
     * <p>Посылает интент указанному пакету с указанным юри</p>
     * @param pack Имя пакета приложения в который нужно отправить интент
     * @param uri Путь передаваемый интенту
     */
    public boolean openInApp(String pack, String uri){
        //TO DO
        return true;
    }

    /**
     * <p>Посылает интент указанному пакету с указанным юри</p>
     * @param pack Имя пакета приложения в который нужно отправить интент
     */
    public boolean appInstalled(String pack){
        //TO DO
        return true;
    }

    /**
     * <p>Перечисление для работы с действиями</p>
     */
    enum Action {
        loadUrl,
        locale,
        appInstalled,
        openInApp,
        Empty
    }
}
