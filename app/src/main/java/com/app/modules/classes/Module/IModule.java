package com.app.modules.classes.Module;

import android.content.Intent;

import com.app.modules.classes.common.Callback;

import org.json.JSONArray;

/**
 * <p>Интерфейс, который будут реализовать все модули приложения</p>
 * <p>Сам интерфейс содержит в себе методы, которые могут частично быть реализованы в модулях</p>
 */
public interface IModule {

    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */
    void pause();

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    void create();

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */
    void resume();

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    void destroy();

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    void activityResult(int requestCode, int resultCode, Intent data);

    /**
     * <p>Входная точка для выполнения действия на подобии CLI.
     * Обрабатываються только действия с определенными названиями по сценариям.</p>
     * @param cb Коллбек, который вызываеться после того как модуль выполнит необходимые действия.
     * @param actionName Название действия, которое соответствует полю из перечисления действий в данном модуле
     * @param args Аргументы к действию
     */
    void execute(Callback cb, String actionName, JSONArray args);


}
