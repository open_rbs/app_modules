package com.app.modules.classes.common;

import android.app.Activity;
import android.app.admin.DnsEvent;
import android.bluetooth.BluetoothClass;

import com.app.modules.classes.Module.Module;
import com.app.modules.classes.ads_module.AdsModule;
import com.app.modules.classes.app_module.AppModule;
import com.app.modules.classes.background_module.BackgroundModule;
import com.app.modules.classes.browser_module.BrowserModule;
import com.app.modules.classes.browser_module.BrowserModule.Browser;
import com.app.modules.classes.device_module.DeviceModule;
import com.app.modules.classes.file_module.FileModule;
import com.app.modules.classes.purchase_module.PurchaseModule;
import com.app.modules.classes.storage_module.StorageModule;

/**
 * Главный класс приложения
 * Системный класс для управления веб-контентом
 */
public class AppSystem {
    public static String IFRAME_SIGN = "#sign_frame";
    private Browser browser;
    private Activity activity;


    public AppSystem(Activity _activity){
        activity = _activity;
        Module.init(activity);

        new Mover();

        init();

        browser = new Browser(Browser.LAYOUT);

        new Loader();
    }

    private void init(){
        FileModule.getInstance();
        AdsModule.getInstance();
        BackgroundModule.getInstance();
        BrowserModule.getInstance();
        DeviceModule.getInstance();
        AppModule.getInstance();
        PurchaseModule.getInstance();
        StorageModule.getInstance();
    }

    /**
     * Открывает урл в основном веб-вью
     * @param url
     */
    public void open(String url){

    }

}
