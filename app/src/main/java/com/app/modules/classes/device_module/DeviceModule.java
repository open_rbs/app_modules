package com.app.modules.classes.device_module;

import android.content.Intent;
import android.util.Log;

import com.app.modules.classes.Module.IModule;
import com.app.modules.classes.Module.Module;
import com.app.modules.classes.common.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * <p>Класс - модуль для работы с устройством</p>
 * <p>С его помощью будут отловлены события нажатия на клавишу назад,
 * открытие и исчезновение клавиатура и тд.</p>
 * <p>Класс наследует Module и реализует интерфейс IModule</p>
 * <p>Реализует паттерн синглтон - должен быть всего 1 екземпляр каждого модуля</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 * @see Module
 * @see IModule
 */
public class DeviceModule extends Module implements IModule{

    /**
     * <p>инстанс данного класса</p>
     */
    private static DeviceModule instance;

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "DEVICE_MODULE";

    /**
     * <p>События генерируемые модулем</p>
     */
    public static final String BACK_PRESSED = "back_pressed";
    public static final String KEYBOARD_ONSHOW = "keyboard_onshow";
    public static final String KEYBOARD_ONHIDE = "keyboard_onhide";

    /**
     * <p>Приватный конструктор для реализации синглтона</p>
     */
    private DeviceModule(){

    }

    /**
     * <p>Метод для реализации синглтона</p>
     * @return каждый модуль должен возвращать свой инстанс в виде своего класса
     */
    public static DeviceModule getInstance() {
        if(instance == null){
            instance = new DeviceModule();
        }
        return instance;
    }

    /**
     * <p>Генерирует евент клика на кнопку назад</p>
     */
    public void backPressed(){
        once(NAME, BACK_PRESSED, "");
    }

    /**
     * <p>Генерирует евенты открытия - закрытия клавиатуры</p>
     */
    public void setKeyboardListener(){
        //TO DO
        //once(NAME, KEYBOARD_ONSHOW, "");
        //once(NAME, KEYBOARD_ONHIDE, "");
    }

    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */
    @Override
    public void pause() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    @Override
    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */
    @Override
    public void resume() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    @Override
    public void destroy() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    public void activityResult(int requestCode, int resultCode, Intent data){

    }

    /**
     * <p>Входная точка для выполнения действия на подобии CLI.
     * Обрабатываються только действия с определенными названиями по сценариям.</p>
     * @param cb Коллбек, который вызываеться после того как модуль выполнит необходимые действия.
     * @param actionName Название действия, которое соответствует полю из перечислением
     * @param args Аргументы к действию
     * @see DeviceModule.Action
     */
    @Override
    public void execute(Callback cb, String actionName, JSONArray args) {
        //родительскому классу присваивается коллбек
        this.callback = cb;

        //пустое действие по умолчанию
        DeviceModule.Action action = DeviceModule.Action.Empty;

        //пытаемся связать строковое название действия с перечислением
        try {
            action = DeviceModule.Action.valueOf(actionName);
        } catch (IllegalArgumentException e) {
            Log.e("PLUGIN_"+NAME, "unexpected error", e);
        }

        //выполняем действие
        try{
            //Извлекаем обьект из аргументов
            JSONObject obj = args.getJSONObject(0);

            //определяем нужный кейс
            switch(action){
                case once:
                    //NOTHING TO DO
                    break;
            }
        }catch(JSONException e){
            e.printStackTrace();
            //в случае ошибки сообщаем в коллбек
            cb.error("Some error with action " + actionName);
        }
    }

    /**
     * <p>Перечисление для работы с действиями</p>
     */
    enum Action {
        once,
        Empty
    }
}
