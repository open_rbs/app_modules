package com.app.modules.classes.purchase_module;

import android.content.Intent;
import android.util.Log;

import com.app.modules.classes.Module.IModule;
import com.app.modules.classes.Module.Module;
import com.app.modules.classes.common.Callback;
import com.app.modules.classes.storage_module.StorageModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * <p>Класс - модуль для работы с покупками в приложении</p>
 * <p>С его помощью достаеться информация про товары в приложении, происходит покупка, потребление
 * товаров и подписок, а также валидация</p>
 * <p>Класс наследует Module и реализует интерфейс IModule</p>
 * <p>Реализует паттерн синглтон - должен быть всего 1 екземпляр каждого модуля</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 * @see Module
 * @see IModule
 */
public class PurchaseModule extends Module implements IModule{

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "PURCHASE_MODULE";
    public static final String BASE64_PURCHASE_KEY = "key from google play";
    public static final String VALIDATOR_URL = "url";

    /**
     * <p>инстанс данного класса</p>
     */
    private static PurchaseModule instance;

    /**
     * <p>Приватный конструктор для реализации синглтона</p>
     */
    private PurchaseModule(){

    }

    /**
     * <p>Метод для реализации синглтона</p>
     * @return каждый модуль должен возвращать свой инстанс в виде своего класса
     */
    public static PurchaseModule getInstance() {
        if(instance == null){
            instance = new PurchaseModule();
        }
        return instance;
    }

    private void init(final List<String> skus){

    }

    private void buy(final String sku, final String developerPayload){

    }

    private void subscribe(final String sku, final String developerPayload, final List<String> oldPurchasedSkus){

    }

    private JSONArray getAvailableProducts(){
        return null;
    }

    private JSONArray getProductDetails(final List<String> skus){
        return  null;
    }

    private void ConsumeProducts(JSONArray arr){

    }

    public void Dispose(){

    }

    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */
    @Override
    public void pause() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    @Override
    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */
    @Override
    public void resume() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    @Override
    public void destroy() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    public void activityResult(int requestCode, int resultCode, Intent data){

    }

    /**
     * <p>Входная точка для выполнения действия на подобии CLI.
     * Обрабатываються только действия с определенными названиями по сценариям.</p>
     * @param cb Коллбек, который вызываеться после того как модуль выполнит необходимые действия.
     * @param actionName Название действия, которое соответствует полю из перечислением
     * @param args Аргументы к действию
     * @see PurchaseModule.Action
     */
    @Override
    public void execute(Callback cb, String actionName, JSONArray args) {
        //родительскому классу присваивается коллбек
        this.callback = cb;

        //пустое действие по умолчанию
        PurchaseModule.Action action = PurchaseModule.Action.Empty;

        //пытаемся связать строковое название действия с перечислением
        try {
            action = PurchaseModule.Action.valueOf(actionName);
        } catch (IllegalArgumentException e) {
            Log.e("PLUGIN_"+NAME, "unexpected error", e);
        }

        //выполняем действие
        try{
            //Извлекаем обьект из аргументов
            JSONObject obj = args.getJSONObject(0);

            //определяем нужный кейс
            switch(action){
                case init:

                    break;
                case buy:

                    break;
                case subscribe:

                    break;
                case getAvailableProducts:

                    break;
            }
        }catch(JSONException e){
            e.printStackTrace();
            //в случае ошибки сообщаем в коллбек
            cb.error("Some error with action " + actionName);
        }
    }

    /**
     * <p>Перечисление для работы с действиями</p>
     */
    enum Action {
        init,
        buy,
        subscribe,
        getAvailableProducts,
        Empty
    }
}
