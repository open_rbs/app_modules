package com.app.modules.classes.common;

import android.app.ProgressDialog;
import android.content.res.Resources;

/**
 * <p>Вспомогательный класс с утилитами</p>
 */
public class Helper {

    /**
     * Прогресс диалог
     */
    private static ProgressDialog pd;
    private static final String TITLE_DIALOG = "Loading...";
    private static final String MESSAGE_DIALOG = "Please wait";

    /**
     * Делает первую букву строки заглавной
     * @param value строка
     * @return измененную строку с первой заглавной буквой
     */
    public static String capitalizeFirst(String value){
        return "";
    }

    /**
     * Создает и показывает лоадинг диалог
     */
    public static void showLoading(){

    }

    /**
     * Скрывает лоадинг диалог и удаляет его
     */
    public static void hideLoading(){
        //pd.dismiss()
    }

    /**
     * Возвращает айди ресурса иконки приложения
     * @param res ресурсы контекста
     * @param icon имя иконки
     * @param type mipmap или drawable
     * @param pkgName имя пакета
     * @return айди ресурка иконки
     */
    public static int getIconResId(Resources res, String icon, String type, String pkgName){
        return 0;
    }
}
