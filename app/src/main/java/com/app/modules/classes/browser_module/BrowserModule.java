package com.app.modules.classes.browser_module;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.app.modules.classes.Module.IModule;
import com.app.modules.classes.Module.Module;
import com.app.modules.classes.background_module.BackgroundModule;
import com.app.modules.classes.common.Callback;
import com.app.modules.classes.storage_module.StorageModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BrowserModule extends Module implements IModule{

    /**
     * <p>События генерируемые модулем</p>
     */
    private static final String ERROR_EVENT = "";
    private static final String START_EVENT = "";
    private static final String STOP_EVENT = "";
    private static final String EXIT_EVENT = "";

    //внутренний браузер
    private Browser browser;

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "STORAGE_MODULE";

    /**
     * <p>инстанс данного класса</p>
     */
    private static BrowserModule instance;

    /**
     * <p>Приватный конструктор для реализации синглтона</p>
     */
    private BrowserModule(){
        browser = new Browser(Browser.DIALOG);
    }

    /**
     * <p>Метод для реализации синглтона</p>
     * @return каждый модуль должен возвращать свой инстанс в виде своего класса
     */
    public static BrowserModule getInstance() {
        if(instance == null){
            instance = new BrowserModule();
        }
        return instance;
    }


    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */
    @Override
    public void pause() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    @Override
    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */
    @Override
    public void resume() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    @Override
    public void destroy() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    public void activityResult(int requestCode, int resultCode, Intent data){

    }


    /**
     * <p>Входная точка для выполнения действия на подобии CLI.
     * Обрабатываються только действия с определенными названиями по сценариям.</p>
     * @param cb Коллбек, который вызываеться после того как модуль выполнит необходимые действия.
     * @param actionName Название действия, которое соответствует полю из перечислением
     * @param args Аргументы к действию
     * @see BrowserModule.Action
     */
    @Override
    public void execute(Callback cb, String actionName, JSONArray args) {
        //родительскому классу присваивается коллбек
        this.callback = cb;

        //пустое действие по умолчанию
        BrowserModule.Action action = BrowserModule.Action.Empty;

        //пытаемся связать строковое название действия с перечислением
        try {
            action = BrowserModule.Action.valueOf(actionName);
        } catch (IllegalArgumentException e) {
            Log.e("PLUGIN_BG", "unexpected error", e);
        }

        //выполняем действие
        try{
            //Извлекаем обьект из аргументов
            JSONObject obj = args.getJSONObject(0);

            //определяем нужный кейс
            switch(action){
                case init:
                    boolean show =  obj.getBoolean("show");
                    browser.init(show);
                    cb.success();

                    break;
                case close:
                    browser.close();
                    cb.success();
                    break;
                case insertCSS:
                    String css = obj.getString("data");
                    browser.insertCSS(css);
                    cb.success();
                    break;
                case show:
                    browser.show();
                    cb.success();
                    break;
                case executeScript:
                    String js = obj.getString("data");
                    browser.executeScript(js);
                    break;
                case loadURL:
                    String url = obj.getString("url");
                    browser.loadURL(url);
                    cb.success();
                    break;
                case once:
                    //NOTHING TO DO
                    break;
            }
        }catch(JSONException e){
            e.printStackTrace();
            //в случае ошибки сообщаем в коллбек
            cb.error("Some error with action " + actionName);
        }
    }

    /**
     * Создает браузер для работы с веб вью и веб контентом
     */
    public static class Browser{

        private WebView inAppWebView;
        private Dialog dialog;
        private boolean show;

        private int currentType;
        public static final int DIALOG = 1;
        public static final int LAYOUT = 2;

        public Browser(int type){
            currentType = type;
        }

        /**
         * Инициализирует webview браузер
         * @param show показывать ли изначально браузер
         */
        public void init(boolean show){

        }

        /**
         * Вставляет ксс на текущю страницу
         * @param css строка ксс для вставки
         */
        public void insertCSS(String css){

        }

        /**
         * закрывает диалог
         */
        public void close(){

        }

        /**
         * Показывает диалог
         */
        public void show(){

        }

        /**
         * Вставляет и исполняет джаваскрипт на текущю страницу
         * @param js строка ксс для вставки
         */
        public void executeScript(String js){

        }

        /**
         * Загрузка страницы
         * @param url
         */
        public void loadURL(String url){

        }

        /**
         * Для вставки ксс и джаваскрипта
         * @param source
         * @param jsWrapper
         * @param isJs
         */
        private void injectDeferredObject(String source, String jsWrapper, boolean isJs){

        }

    }

    enum Action {
        init,
        insertCSS,
        close,
        show,
        executeScript,
        loadURL,
        once,
        Empty
    }

    class MyWebClient{
        private View myCustomView;
        private WebChromeClient.CustomViewCallback mCustomViewCallback;
        private FrameLayout mFullscreenContainer;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        public MyWebClient(){

        }

        public Bitmap getDefaultVideoPoster(){
            return null;
        }

        public void onHideCustomView(){

        }

        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback){

        }
    }

    class JavaScriptHandler{
        public JavaScriptHandler(Context context){

        }

        public void callback(String cib, String res){

        }
    }

    class InAppBrowserClient{
        public InAppBrowserClient(){

        }

        public void onPageStarted(WebView view, String url, Bitmap favicon){
            //generate event
        }

        public void onPageFinished(WebView view, String url){
            //generate event
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl){
            //generate event
        }
    }
}
